module MP3Player {
    requires javafx.graphics;
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.media;
    requires jid3lib;

    exports pl.lucajones.mp3player.main to javafx.graphics;
    opens pl.lucajones.mp3player.controller to javafx.fxml;
    opens pl.lucajones.mp3player.mp3 to javafx.base;
}